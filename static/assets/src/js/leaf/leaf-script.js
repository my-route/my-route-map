var map_visit_list = [
    {
        "id": 1,
        "time_from": "19.06.2018 00:00",
        "time_to": "20.06.218 23:59",
        "description": "my description"
    },
    {
        "id": 2,
        "time_from": "20.06.2018 00:00",
        "time_to": "21.06.218 23:59",
        "description": "my description 1"
    },
    {
        "id": 3,
        "time_from": "21.06.2018 00:00",
        "time_to": "22.06.218 23:59",
        "description": "my description 2"
    },
    {
        "id": 4,
        "time_from": "23.06.2018 00:00",
        "time_to": "24.06.218 23:59",
        "description": "my description 3"
    },
    {
        "id": 5,
        "time_from": "25.06.2018 00:00",
        "time_to": "26.06.218 23:59",
        "description": "my description 4"
    }
];

var map_point_list = [
    {
        "map_id": 2,
        "title": "Title 2",
        "description": "Description 2",
        "lat": "54.54618125397208",
        "lon": "39.796772003173835",
        "marker_instance": undefined,
    },
    {
        "map_id": 3,
        "title": "Title 3",
        "description": "Description 3",
        "lat": "54.63609471614411",
        "lon": "39.82234954833985",
        "marker_instance": undefined
    },
    {
        "map_id": 4,
        "title": "Title 4",
        "description": "Description 4",
        "lat": "54.629040109837995",
        "lon": "39.55575942993165",
        "marker_instance": undefined
    },
];

function updateListVisit() {
    var html = '';
    $.each(map_visit_list, function (k, item) {
        html += '<div class="form-group list-group-item list-group-item-action flex-column align-items-start">' +
            '<div class="d-flex w-100 justify-content-between">' +
            '<a class="font-weight-bold mb-1" href="#"><h6>Visit ' + item.time_from + ' - ' + item.time_to + '</h6></a>' +
            '</div>' +
            '<button class="btn btn-outline-primary visits-edit-btn" data-tab="visit-edit" data-id="' + item.id + '"><i class="fa fa-pencil"></i></button>&nbsp;' +
            '<button class="btn btn-outline-danger visits-remove-btn" data-id="' + item.id + '"><i class="fa fa-trash"></i></button>' +
            '</div>';
    });
    $("#visits-list-block").html(html);
}

function updateListPosition() {
    var html = '';
    $.each(map_point_list, function (k, item) {
        html += '<div class="form-group list-group-item list-group-item-action flex-column align-items-start">' +
            '<div class="d-flex w-100 justify-content-between">' +
            '<a data-map_id="' + item.map_id + '" href="#"><h6>' + item.title + '</h6></a>' +
            '</div>' +
            '<p class="mb-1">' + item.description + '</p>' +
            '<button class="btn btn-outline-primary map-point-edit-btn" data-tab="map-point-edit" data-map_id="' + item.map_id + '"><i class="fa fa-pencil"></i> Change entry </button>' +
            '<button class="btn btn-outline-primary map-point-visit-change-btn" data-tab="map-point-visit-change" data-map_id="' + item.map_id + '"><i class="fa fa-pencil"></i> Change visit </button>' +
            '<button class="btn btn-outline-danger map-point-visit-remove-btn" data-map_id="' + item.map_id + '"><i class="fa fa-trash"></i></button></div>';
    });
    $("#points-list-block").html(html);
}

updateListVisit();
updateListPosition();

L.Control.Watermark = L.Control.extend({
    onAdd: function (map) {
        var img = L.DomUtil.create('img');

        img.src = '/static/assets/images/sign2.png';
        img.style.width = '80px';

        return img;
    },

    onRemove: function (map) {
        // Nothing to do here
    }
});
L.control.watermark = function (opts) {
    return new L.Control.Watermark(opts);
};

// -----
function mapVisit(time_from, time_to, description, visit_id) {
    $(".visit-edit #time_from").val(time_from);
    $(".visit-edit #time_to").val(time_to);
    $(".visit-edit #description").val(description);
    $(".visit-edit #visit_id").val(visit_id);
}

function mapPoint(lat, lon, title, description, map_id) {
    $(".map-point-edit #lat").val(lat);
    $(".map-point-edit #lon").val(lon);
    $(".map-point-edit #title").val(title);
    $(".map-point-edit #description").val(description);
    $(".map-point-edit #map_id").val(map_id);
}

// ----
$("#visits_add").click(function () {
    $(".visit-edit input").val("");
    $(".visit-edit textarea").val("");
});

$("#map_point_add").click(function () {
    $(".map-point-edit input").val("");
    $(".map-point-edit textarea").val("");
});

function deleteVisit(visit_id) {
    var index = -1;
    $.each(map_visit_list, function (k, item) {
        if (item.id === visit_id) {
            index = k;
            return false;
        }
    });

    if (index > -1) {
        map_visit_list.splice(index, 1);
    }
}

function deleteMarker(map, map_id) {
    var index = -1;
    $.each(map_point_list, function (k, item) {
        if (item.map_id === map_id) {
            index = k;
            map.removeLayer(item.marker_instance);
            return false;
        }
    });

    if (index > -1) {
        map_point_list.splice(index, 1);
    }
}

var customControl = L.Control.extend({

    options: {
        position: 'topright',
        content: '<a class="leaflet-point-marker leaflet-bar-part" href="#" title="Point add to map" style="outline: none;"></a>',
        content_active: '<a class="leaflet-point-marker-active leaflet-bar-part" href="#" title="Point add to map" style="outline: none;"></a>',
        classes: 'leaflet-point-marker leaflet-bar leaflet-control'
    },
    onAdd: function (map) {
        _this = this;

        var container = L.DomUtil.create('div');
        container.className = _this.options.classes;
        container.innerHTML = _this.options.content;

        _this._map = map;
        _this.is_click_btn = false;
        _this._is_add_marker = false;

        _this._update(map, _this);

        container.onmouseover = function () {
        };
        container.onmouseout = function () {
        };

        container.onclick = function () {
            _this._is_add_marker = !_this._is_add_marker;
            if (_this._is_add_marker) {
                container.innerHTML = _this.options.content_active
            } else {
                container.innerHTML = _this.options.content
            }
            _this.is_click_btn = true;
        };


        return container;
    },

    _update: function (map, _this) {
        map.on('click', this._onMapClick, _this);
    },

    _onMapClick: function (e) {
        var _this = this;
        if (_this.is_click_btn) {
            _this.is_click_btn = false;
            return false
        }
        if (!_this._is_add_marker) return false;

        var data = {
            "map_id": randomInteger(0, 9999),
            'lat': e.latlng.lat,
            'lon': e.latlng.lng,
            'title': 'New point',
            'description': 'Description new point',
            "marker_instance": undefined,
        };

        var marker = addPoint(_this._map, data);
        marker.addTo(_this._map);

        data.marker_instance = marker;

        map_point_list.push(data);

        updateListPosition();
    }

});


function addPoint(map, data) {
    if (!data) return false;

    var geojsonFeature = {
        "type": "Feature",
        "properties": {},
        "geometry": {
            "type": "Point",
            "coordinates": [data.lat, data.lon]
        }
    };

    var _onPopupOpen = function () {
        var _thisInner = this;
        // To remove marker on click of delete
        $(".marker-delete-button:visible").click(function () {
            deleteMarker(map, $(this).data('map_id'));
            updateListPosition();
            // map.removeLayer(_thisInner);
        });
        $('.edit_marker:visible').click(function () {
            var index = findIndexPoint(map_point_list, $(this).data("map_id"));
            if (index !== false) {
                mapPoint(
                    _thisInner.getLatLng().lat,
                    _thisInner.getLatLng().lng,
                    map_point_list[index].title,
                    map_point_list[index].description,
                    $(this).data('map_id')
                );
            }
        });
    };

    var marker = L.marker([data.lat, data.lon], {
        title: data.title,
        alt: data.title,
        riseOnHover: true,
        draggable: true
    }).bindPopup(tmpl_popup_content(data));
    marker.on("popupopen", _onPopupOpen);

    return marker;
}


navigator.geolocation.getCurrentPosition(function (location) {

    var cities = L.layerGroup();

    var mbAttr = 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
        '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
        'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        mbUrl = 'https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw';

    var grayscale = L.tileLayer(mbUrl, {id: 'mapbox.light', attribution: mbAttr}),
        streets = L.tileLayer(mbUrl, {id: 'mapbox.streets', attribution: mbAttr}),
        satellite = L.tileLayer(mbUrl, {id: 'mapbox.satellite', attribution: mbAttr});

    var latlng = new L.LatLng(location.coords.latitude, location.coords.longitude);
    var map = L.map('map', {
        zoomControl: false,
        layers: [streets, cities]
    }).setView(latlng, 13);

    var baseLayers = {
        "Streets": streets,
        "Grayscale": grayscale,
        "Satellite": satellite
    }, overlays = {
        "Cities": cities
    };

    L.control.layers(baseLayers, overlays).addTo(map);

    L.control.zoom({position: 'topright'}).addTo(map);

    L.control.mousePosition().addTo(map);

    // -------------
    var control = new L.Control.Geocoder({geocoder: L.Control.Geocoder.nominatim()});
    control.addTo(map);
    //------------
    L.control.watermark({position: 'bottomright'}).addTo(map);

    map.addControl(new L.Control.Fullscreen({position: 'topright'}));
    map.addControl(new customControl());

    var sidebar = L.control.sidebar({
        autopan: false,       // whether to maintain the centered map point when opening the sidebar
        closeButton: true,    // whether t add a close button to the panes
        container: 'sidebar', // the DOM container or #ID of a predefined sidebar container that should be used
        position: 'left',     // left or right
    });
    map.addControl(sidebar);

    // инициализация существующих точек
    $.each(map_point_list, function (k, item) {
        item.marker_instance = addPoint(map, {
            "map_id": item.map_id,
            'lat': item.lat,
            'lon': item.lon,
            'title': item.title,
            'description': item.description
        });
        item.marker_instance.addTo(map);
    });

    $(".visit-edit .save").on('click', function () {
        var visit_id = $(".visit-edit #visit_id").val();
        var data = {
            'id': visit_id !== '' ? parseInt(visit_id) : randomInteger(0, 9999),
            'time_from': $(".visit-edit #time_from").val(),
            'time_to': $(".visit-edit #time_to").val(),
            'description': $(".visit-edit #description").val(),
        };
        var index = findVisitPoint(map_visit_list, data.id);
        if (index !== false) {
            map_visit_list[index].id = data.id;
            map_visit_list[index].time_from = data.time_from;
            map_visit_list[index].time_to = data.time_to;
            map_visit_list[index].description = data.description;
        } else {
            addVisit(data);
        }

        updateListVisit();
    });

    $(".map-point-edit .save").on('click', function () {
        var map_id = $(".map-point-edit #map_id").val();
        var data = {
            'map_id': map_id !== '' ? parseInt(map_id) : randomInteger(0, 9999),
            'lat': $(".map-point-edit #lat").val(),
            'lon': $(".map-point-edit #lon").val(),
            'title': $(".map-point-edit #title").val(),
            'description': $(".map-point-edit #description").val(),
        };
        var index = findIndexPoint(map_point_list, data.map_id);
        if (index !== false) {
            map_point_list[index].title = data.title;
            map_point_list[index].lat = data.lat;
            map_point_list[index].lon = data.lon;
            map_point_list[index].description = data.description;
            map_point_list[index].marker_instance.getPopup().setContent(tmpl_popup_content(data))
            // map_point_list[index].marker_instance._updateLayout();
            // map_point_list[index].marker_instance._updatePosition();
        } else {
            addPoint(map, data);
        }

        updateListPosition();
    });


    $(document).on('click', '.edit_marker:visible', function () {
        $(".map-point-wrap-block .map_point").hide();
        $(".map-point-wrap-block .map-point-edit").show();
        sidebar.open('map_route');
    });

    $('.visit-edit .cancel').click(function (e) {
        e.preventDefault();
        $(".visit-edit input").val("");
        $(".visit-edit textarea").val("");
        return false;
    });

    $('.map-point-edit .cancel').click(function (e) {
        e.preventDefault();
        $(".map-point-edit input").val("");
        $(".map-point-edit textarea").val("");
        return false;
    });

    function markerLocate(map_id) {
        $.each(map_point_list, function (k, item) {
            if (parseInt(item.map_id) === parseInt(map_id)) {
                map.setView([item.lat, item.lon], 13);
                // markers[i].openPopup();
            }
        });
    }

    $(document).on("click", "#points-list-block a", function () {
        markerLocate($(this).data("map_id"));
    });

    $(document).on("click", "#visits-list-block .visits-remove-btn", function (e) {
        e.preventDefault();
        // удаляются так же на сервере визит и точки привязанные к визиту
        deleteVisit($(this).data("id"), map_visit_list);
        updateListVisit();
        updateListPosition();

        return false;
    });

    $(document).on("click", "#points-list-block .map-point-visit-remove-btn", function () {

        deleteMarker(map, $(this).data("map_id"), map_point_list);

        updateListPosition();

        return false;
    });

    $(document).on("click", ".visits-edit-btn", function () {
        var visit_id = $(this).data("id");
        $.each(map_visit_list, function (k, item) {
            if (parseInt(item.id) === parseInt(visit_id)) {
                mapVisit(item.time_from, item.time_to, item.description, item.id);
                return false;
            }
        });
        return false;
    });

    $(document).on("click", ".map-point-edit-btn", function () {
        var map_id = $(this).data("map_id");
        $.each(map_point_list, function (k, item) {
            if (parseInt(item.map_id) === parseInt(map_id)) {
                mapPoint(item.lat, item.lon, item.title, item.description, item.map_id);
                return false;
            }
        });
        return false;
    });

});

function addVisit(data) {
    map_visit_list.push(data);
    return data
}

function savePosition(data) {
    map_point_list.push({
        "map_id": data.map_id,
        "title": data.title,
        "description": data.description,
        "lat": data.lat,
        "lon": data.lon,
        "marker_instance": data.marker_instance
    })
}

function randomInteger(min, max) {
    return Math.round(min - 0.5 + Math.random() * (max - min + 1));
}

var tmpl_popup_content = function (data) {
    return "<h5>" + data.title + "</h5>" +
        "<p>" + data.description + "</p>" +
        "<p>Change position to visit <a href='#'>visit 1</a> <a href='#'>save</a><br>" +
        "Edit position <a href='#' class='edit_marker' data-map_id='" + data.map_id + "'>edit</a></p>" +
        "<input type='button' value='Delete this marker' data-map_id='" + data.map_id + "' class='marker-delete-button'/>"
};

function findIndexPoint(_map_point_list, map_id) {
    var index = -1;
    $.each(_map_point_list, function (k, item) {
        if (item.map_id === map_id) {
            index = k;
            return false;
        }
    });
    return index > -1 ? index : false;
}

function findVisitPoint(_map_visit_list, visit_id) {
    var index = -1;
    $.each(_map_visit_list, function (k, item) {
        if (item.id === visit_id) {
            index = k;
            return false;
        }
    });
    return index > -1 ? index : false;
}