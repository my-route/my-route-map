$(function () {

    $(document).on("click", ".visits-control-block button", function () {
        $(".visit-wrap-block .visits").hide();
        var tab = $(this).data("tab");
        $("." + tab).show();
    });

    $(document).on("click", ".visits-edit-btn", function () {
        var tab = $(this).data("tab");
        $("." + tab).show();
        $(".visit-list").hide();
    });
    $(".visit-edit .cancel").click(function () {
        $(".visit-edit").hide();
        $(".visit-list").show();
    });

    // -- map point ---
    $(document).on("click",".map-point-control-block a, .map-point-control-block button", function () {
        $(".map-point-wrap-block .map_point").hide();
        var tab = $(this).data("tab");
        $("." + tab).show();
    });

    $(document).on("click", ".map-point-edit-btn", function () {
        var tab = $(this).data("tab");
        $("." + tab).show();
        $(".map-point-list").hide();
    });

    $(document).on("click", ".map-point-edit .cancel", function () {
        $(".map-point-edit").hide();
        $(".map-point-list").show();
    });

    // -- map point change visit --
    $(document).on("click", ".map-point-visit-change-btn", function () {
        var tab = $(this).data("tab");
        $("." + tab).show();
        $(".map-point-list").hide();
    });
    $(".map-point-visit-change .cancel").click(function () {
        $(".map-point-visit-change").hide();
        $(".map-point-list").show();
    });

});