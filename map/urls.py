from map.views import MapPointViewSet, VisitViewSet, VisitMapPointLinkViewSet
from django.urls import path, include
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register('map-points', MapPointViewSet)
router.register('visits', VisitViewSet)
router.register('visits-links', VisitMapPointLinkViewSet)

urlpatterns = [
    path('v1/', include(router.urls)),
]
