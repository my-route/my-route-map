from rest_framework import serializers, permissions
from .models import MapPoint, Visit, VisitMapPointLink


class MapPointSerializer(serializers.ModelSerializer):
    permission_classes = (permissions.IsAuthenticated,)

    class Meta:
        model = MapPoint
        fields = ('id', 'user', 'title', 'lat', 'lon', 'description')


class VisitSerializer(serializers.ModelSerializer):
    permission_classes = (permissions.IsAuthenticated,)

    class Meta:
        model = Visit
        fields = ('id', 'user', 'time_from', 'time_to')


class VisitMapPointLinkSerializer(serializers.ModelSerializer):
    permission_classes = (permissions.IsAuthenticated,)

    class Meta:
        model = VisitMapPointLink
        fields = ('id', 'visit', 'map_point')
