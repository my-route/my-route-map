from django.db import models
from django.contrib.auth.models import User

from django.utils import timezone

class MapPoint(models.Model):
    """
    Map of points for route
    """
    user = models.ForeignKey(User, on_delete=models.CASCADE, blank=False, null=False)
    title = models.CharField(default='', max_length=255)
    lat = models.CharField(default='', max_length=150)
    lon = models.CharField(default='', max_length=150)
    description = models.TextField(default='')

    class Meta:
        verbose_name = "Map point"
        verbose_name_plural = "Map points"

    def __unicode__(self):
        return self.title


class Visit(models.Model):
    """
    Visit of point
    """
    import datetime
    user = models.ForeignKey(User, on_delete=models.CASCADE, blank=False, null=False)
    time_from = models.DateTimeField(default=datetime.datetime.now(), blank=True)
    time_to = models.DateTimeField(default=datetime.datetime.now(), blank=True)

    class Meta:
        verbose_name = "Visit of point"
        verbose_name_plural = "Visits of points"

    def __unicode__(self):
        return str(self.time_from) + " " + str(self.time_to)


class VisitMapPointLink(models.Model):
    visit = models.ForeignKey(Visit, on_delete=models.CASCADE, blank=False, null=False)
    map_point = models.ForeignKey(MapPoint, on_delete=models.CASCADE, blank=False, null=False)
