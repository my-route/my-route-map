from rest_framework import viewsets
from .serializers import MapPointSerializer, VisitSerializer, VisitMapPointLinkSerializer
from .models import MapPoint, Visit, VisitMapPointLink
from rest_framework.decorators import action

from rest_framework.response import Response
from rest_framework import status


class MapPointViewSet(viewsets.ModelViewSet):
    queryset = MapPoint.objects.all()
    serializer_class = MapPointSerializer

    def create(self, request, *args, **kwargs):
        my_data = {
            "user": request.user.pk,
            "title": request.data['title'],
            "lat": request.data['lat'],
            "lon": request.data['lon'],
            "description": request.data['description']
        }
        serializer = self.get_serializer(data=my_data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def list(self, request, *args, **kwargs):
        queryset = MapPoint.objects.filter(user_id=request.user.pk)
        serializer = MapPointSerializer(queryset, many=True)
        return Response(serializer.data)


class VisitViewSet(viewsets.ModelViewSet):
    queryset = Visit.objects.all()
    serializer_class = VisitSerializer

    def create(self, request, *args, **kwargs):
        my_data = {
            "user": request.user.pk,
            "time_from": request.data['time_from'],
            "time_to": request.data['time_to']
        }
        serializer = self.get_serializer(data=my_data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def list(self, request, *args, **kwargs):
        queryset = Visit.objects.filter(user_id=request.user.pk)
        serializer = VisitSerializer(queryset, many=True)
        return Response(serializer.data)

    @action(methods=['get'], detail=True, url_path='map-points', url_name='map_points')
    def visit_list(self, request, pk=None):
        serializer = MapPointSerializer(
            MapPoint.objects.filter(visitmappointlink__visit__id=pk, user_id=request.user.pk), many=True)
        return Response(serializer.data)


class VisitMapPointLinkViewSet(viewsets.ModelViewSet):
    queryset = VisitMapPointLink.objects.all()
    serializer_class = VisitMapPointLinkSerializer

    def create(self, request, *args, **kwargs):
        my_data = {
            "user": request.user.pk,
            "visit": request.data['visit'],
            "map_point": request.data['map_point']
        }
        serializer = self.get_serializer(data=my_data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def list(self, request, *args, **kwargs):
        queryset = VisitMapPointLink.objects.filter(visit=Visit.objects.filter(user_id=request.user.pk).first())
        serializer = VisitMapPointLinkSerializer(queryset, many=True)
        return Response(serializer.data)
