from map.views import MapPointViewSet, VisitViewSet, VisitMapPointLinkViewSet
from django.urls import path, include
from rest_framework.routers import DefaultRouter

from map_page.views import index_view

urlpatterns = [
    path('', index_view, name='index'),
]
