from django.shortcuts import render


def index_view(request):
    return render(request, 'map_page/index_tmpl.html')
