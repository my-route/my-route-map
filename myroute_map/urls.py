"""myroute_map URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.urls import path, include

import django_cas_ng.views

urlpatterns_map = [
    path('accounts/login/', django_cas_ng.views.login, name='cas_ng_login'),
    path('accounts/logout/', django_cas_ng.views.logout, name='cas_ng_logout'),
    path('accounts/callback/', django_cas_ng.views.callback, name='cas_ng_proxy_callback'),
    path('api/', include('map.urls')),
    path('', include('map_page.urls'))
    # path('admin/', admin.site.urls),
]

urlpatterns = urlpatterns_map
