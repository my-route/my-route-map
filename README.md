# My route application

## Install environment
```
python -m venv environment
```

## Install requirements
```
environment\Scripts\activate
pip install -r requirements
pip install -e git+https://github.com/jbittel/django-mama-cas.git#egg=django_mama_cas
```

## Make migrations
```
manage.py makemigrations
manage.py migrate
```

##install bower npm
```
cd static/assets/
npm install
```



